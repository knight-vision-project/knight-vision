#!/bin/bash
# This one should be used for starting development instances 
# Start React app
cd react-app/
npm start > /dev/null 2>&1 & 

# Store the React app's process ID
REACT_PID=$!

# Define the path to the virtual environment
VENV_DIR="pyenv"
cd ../electron-app/

# Check if the virtual environment exists
if [ ! -d "$VENV_DIR" ]; then
  echo "Virtual environment not found. Creating..."
  
  # Create the virtual environment
  python3 -m venv "$VENV_DIR"
  
  # Activate the virtual environment
  source "$VENV_DIR/bin/activate"
  
  # Install requirements
  python3 -m pip install -r requirements.txt
  
  echo "Virtual environment created and requirements installed."
else
  echo "Virtual environment exists. Skipping creation."
  
  # Activate the virtual environment
  source "$VENV_DIR/bin/activate"
  
  # Install requirements (in case any new requirements were added)
  python3 -m pip install -r requirements.txt
  
  echo "Virtual environment is activated and requirements are up to date."
fi

# Start Electron app
npm start &

# Store the Electron app's process ID
ELECTRON_PID=$!

# Function to stop both apps when the script is terminated
function stop_apps {
  # Terminate React app
  kill $REACT_PID
  
  # Terminate Electron app
  kill $ELECTRON_PID
  
  # Wait for the processes to exit gracefully
  wait $REACT_PID
  wait $ELECTRON_PID
  
  # Exit the script
  exit
}

# Register the function to be called on script termination
trap stop_apps SIGINT SIGTERM

# Wait for script termination
wait
