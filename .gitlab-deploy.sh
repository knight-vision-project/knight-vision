#!/bin/bash
#Get servers list
set -f
string=$CELL_TESTING_MONGODB
array=(${string//,/ })
#Iterate servers for deploy and pull last commit

for j in "${!array[@]}"
do
echo "Deploy project on server ${array[j]}"
ssh -o "StrictHostKeyChecking=no" $CELL_TESTING_MONGODB_USER@${array[j]} '
KNIGHT_VISION=/datadisk/applications/knight_vision
if [ -d "$KNIGHT_VISION/.git" ]
then
cd $KNIGHT_VISION
sudo git pull origin master
sudo git submodule update --recursive --remote
sudo docker-compose -f knight_vision.yml down
#sudo docker build -t knight_vision:$(git log -1 --format=%h) .
sudo docker-compose -f knight_vision.yml build
sudo docker-compose -f knight_vision.yml up -d
else
cd /datadisk/applications/
sudo git clone -b master --recurse-submodules https://deploy_dev:deploy_dev@sc.rpsapi.in/dev/knight_vision.git
cd /datadisk/applications/knight_vision
sudo docker-compose -f knight_vision.yml up -d
fi'
done