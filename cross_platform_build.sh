#!/bin/bash

# Build react application
cd /app/react-app
npm install
npm run build

# Build electron application
cd ../electron-app
npm install

# Build for Linux/Debian
npm run make:linux

# Build for Windows
# npm run make:win32

# Build for MacOS
# npm run make:macos

# Rest permissions 
chmod -R 777 /app/electron-app/out/