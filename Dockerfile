FROM python:3.9

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update -y && apt upgrade -y
RUN apt-get install -y dpkg fakeroot rpm wget 

# Required for cx_freeze
RUN apt-get install -y mingw-w64 patchelf

# Install required packages for building Windows executables
RUN dpkg --add-architecture i386
RUN mkdir -pm755 /etc/apt/keyrings
RUN wget -O /etc/apt/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key
RUN wget -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/debian/dists/bookworm/winehq-bookworm.sources
RUN apt-get update
RUN apt-get install -y --install-recommends winehq-stable
RUN apt-get install -y mono-complete

# Customisation of wine applications
RUN winecfg -v
RUN wine --version
# RUN wget -q https://www.python.org/ftp/python/3.10.11/python-3.10.11-amd64.exe
# RUN wine cmd /c "echo %USERNAME%"
# RUN wine cmd /c "echo %PATH%"

# Install X11 and related packages
# RUN apt-get update && apt-get install -y xorg xserver-xorg xserver-xorg-video-all xinit
# RUN apt-get install wine32 -y
# RUN apt-get install libsm6:i386 -y
# ENV DISPLAY=:0
# RUN wine python-3.10.11-amd64.exe /quiet
# RUN wine reg add 'HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' /f /v Path /t REG_EXPAND_SZ /d "C:\windows\system32;C:\windows;C:\windows\system32\wbem;C:\windows\system32\WindowsPowershell\v1.0;C:\users\root\AppData\Local\Programs\Python\Python310\Scripts\;C:\users\root\AppData\Local\Programs\Python\Python310\;"
# RUN wine cmd /c "CD /root/.wine/drive_c/Program Files && DIR"
# RUN wine python -v

# Install dependencies for MacOS 
RUN apt-get install -y zip

ENV NODE_VERSION=18.16.0
ENV NVM_DIR=/root/.nvm
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
RUN node --version
RUN npm --version

# Set the working directory
WORKDIR /app

# Copy the script to the container
COPY . /app

# Install dependencies if needed
RUN pwd; ls
RUN cd electron-app && python -m pip install -r requirements.txt
RUN cd electron-app/python && python setup.py build

# Set the command to execute the script
CMD ["./cross_platform_build.sh"]
