# Knight Vision Introduction
Welcome to the Knight Vision project. It is an interface for interaction with White Knight which protects our batteries from Lithium calamities

# Release guidelines
Execute the script in docker environment for consistent build
```bash
docker build -t knight_vision_image .
docker run -v "$(pwd)/out:/app/electron-app/out" knight_vision_image
```